# ActivityPub

Each PeerTube instance is able to fetch video from other compatible servers it follows, in a process known as “federation”.
Federation is implemented using the ActivityPub protocol, in order to leverage existing tools and be
compatible with other services such as Mastodon, Pleroma and [many more](https://fediverse.network/).

Federation in PeerTube is twofold: videos metadata are shared as activities for inter-server communication
in what amounts to sharing parts of one's database, and user interaction via comments which are compatible
with the kind of activity textual platforms like Mastodon use.

## Supported Activities

- [Create](#create)
- [Update](#update)
- [Delete](#delete)
- [Follow](#follow)
- [Accept](#accept)
- [Announce](#announce)
- [Undo](#undo)
- [Like](#like)
- [Reject](#reject)

## Supported Objects

- [Video](#video)
- [CacheFile](#cache-file)
- [Note](#note)
- [Flag](#flag)
- [Dislike](#dislike)
- [Playlist](#playlist)
- [PlaylistElement](#playlist-element)
- Actor
- Like

### Follow

Follow is an activity standardized in the ActivityPub specification (see [Follow Activity](https://www.w3.org/TR/activitypub/#follow-activity-inbox)).
The Follow activity is used to subscribe to the activities of another actor (a server
subscribing to another server's videos, a user subscribing to another user's videos).

#### Supported on

- Actor URI

### Accept

#### Supported on

- Follow

### Reject

Reject is an activity standardized in the ActivityPub specification (see [Reject Activity](https://www.w3.org/TR/activitypub/#reject-activity-inbox)).

#### Supported on

- Follow

### Undo

Undo is an activity standardized in the ActivityPub specification (see [Undo Activity](https://www.w3.org/TR/activitypub/#undo-activity-inbox)).
The Undo activity is used to undo a previous activity.

#### Supported on

- [Follow](#follow)
- [Like](#like)
- [Create](#create)
  - [Dislike](#dislike)
  - [CacheFile](#cachefile)
- [Announce](#announce)

### Like

Like is an activity standardized in the ActivityPub specification (see [Like Activity](https://www.w3.org/TR/activitypub/#like-activity-inbox)).

#### Supported on

- [Video](#video)

### Update

Update is an activity standardized in the ActivityPub specification (see [Update Activity](https://www.w3.org/TR/activitypub/#update-activity-inbox)).
The Update activity is used when updating an already existing object.

#### Supported on

- [CacheFile](#cachefile)
- [Video](#video)
- Actor (Person, Application, Group)

### Create

Create is an activity standardized in the ActivityPub specification (see [Create Activity](https://www.w3.org/TR/activitypub/#create-activity-inbox)).
The Create activity is used when posting a new object. This has the side effect
that the `object` embedded within the Activity (in the object property) is created.

#### Supported on

- [CacheFile](#cachefile) object
- [Video](#video) object
- View object
- [Dislike](#dislike) object
- [Note](#note) object (for comments)

#### Example

```json
{
  "@context": [
    "https://www.w3.org/ns/activitystreams",
    "https://w3id.org/security/v1",
    {}
  ],
  "to": ["https://peertube2.cpy.re/accounts/root/activity"],
  "type": "Create",
  "actor": "https://peertube2.cpy.re/accounts/root",
  "object": {}
}
```

### Announce

Announce is an activity standardized in the ActivityPub specification (see [Announce Activity](https://www.w3.org/TR/activitypub/#announce-activity-inbox)).

#### Supported on

- [Video](#video)

#### Example

```json
{
  "type": "Announce",
  "id": "https://peertube2.cpy.re/videos/watch/997111d4-e8d8-4f45-99d3-857905785d05/announces/1",
  "actor": "https://peertube2.cpy.re/accounts/root",
  "object": "https://peertube2.cpy.re/videos/watch/997111d4-e8d8-4f45-99d3-857905785d05",
  "to": [
    "https://www.w3.org/ns/activitystreams#Public",
    "https://peertube2.cpy.re/accounts/root/followers",
    "https://peertube2.cpy.re/video-channels/root_channel/followers"
  ],
  "cc": []
}
```

## Supported Objects

### Video

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object extends the ActivityPub specification, and therefore some properties are not part of it.
</div>

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/video-torrent-object.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/activitypub/objects/video-torrent-object.ts).

[video torrent object](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/activitypub/objects/video-torrent-object.ts ':include :type=code')

### CacheFile

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object is not standard to the ActivityPub specification.
</div>

The object is used to represent a cached file. It is usually sent by third-party servers to the origin server hosting a video
file (a resolution from a `Video`), to notify it that they have put up a copy of that file. The origin server should
then add the server emitting the `CacheFile` to the list of [WebSeeds](http://bittorrent.org/beps/bep_0019.html) for that file.

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/cache-file-object.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/activitypub/objects/cache-file-object.ts).

[cache file object](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/activitypub/objects/cache-file-object.ts ':include :type=code')

### Note

A `Note` is usually a comment made to a video. Since most ActivityPub textual platforms use the `Note` object for their
messages, most of them can interact in the same way with PeerTube videos, making them able to comment PeerTube videos
directly! A `Note` is emitted along the Video publication object: the former is used to notify textual platforms of
the Fediverse, the latter to notify the Vidiverse.

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/video-comment-object.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/activitypub/objects/video-comment-object.ts).

[video comment object](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/activitypub/objects/video-comment-object.ts ':include :type=code')

### Flag

A `Flag` represents a report transferred to a remote instance.

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/abuse-object.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/activitypub/objects/abuse-object.ts).

[abuse object](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/activitypub/objects/abuse-object.ts ':include :type=code')

### Dislike

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object is not standard to the ActivityPub specification.
</div>

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/dislike-object.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/activitypub/objects/dislike-object.ts).

[dislike object](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/activitypub/objects/dislike-object.ts ':include :type=code')

### Playlist

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object is not standard to the ActivityPub specification.
</div>

The object is used to represent a video playlist. It extends [OrderedCollectionPage](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-orderedcollectionpage) and items are [PlaylistElement](#playlist-element).

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/playlist-object.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/activitypub/objects/playlist-object.ts).

[cache file object](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/activitypub/objects/playlist-object.ts ':include :type=code')

### Playlist Element

<div class="alert alert-info" role="alert">
  <strong>Note:</strong> this object is not standard to the ActivityPub specification.
</div>

The object is used to represent a video playlist element.

#### Structure

The model structure definition lies in [shared/models/activitypub/objects/playlist-element-object.ts](https://github.com/Chocobozzz/PeerTube/blob/develop/shared/models/activitypub/objects/playlist-element-object.ts).

[cache file object](https://raw.githubusercontent.com/Chocobozzz/PeerTube/develop/shared/models/activitypub/objects/playlist-element-object.ts ':include :type=code')

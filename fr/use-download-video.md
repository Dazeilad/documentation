# Télécharger une vidéo

Vous pouvez télécharger des vidéos directement depuis l'interface web de PeerTube, en cliquant sur le bouton <i data-feather="more-horizontal"></i> à côté du titre, puis sur l'option "Télécharger". Un choix vous est alors donné quant à la manière dont vous souhaitez télécharger :

![Modale présentant les options de téléchargement](./assets/fr-video-download.png)

* "Téléchargement direct", qui fait ce qu'il dit : votre navigateur web télécharge la vidéo depuis le serveur d'origine de la vidéo.
* Téléchargement en Peer To Peer via "Torrent", où vous avez besoin d'un client compatible WebTorrent pour télécharger la vidéo non seulement depuis le serveur d'origine mais aussi depuis d'autres pairs qui regardent la vidéo ou la partagent depuis leurs propres clients compatibles WebTorrent à la maison ! (en faisant cela, vous aidez le réseau à être plus résilient !) - n'importe quel BitTorrent peut aussi télécharger la vidéo, mais sans le support de WebTorrent, ils ne pourront pas échanger avec les navigateurs web.

**Astuce** : selon l'instance, vous pouvez télécharger la vidéo dans différents formats. Toutefois, veuillez-vous assurer d'avoir obtenu au préalable une licence compatible avec l'utilisation prévue de la vidéo.
